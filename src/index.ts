import express from "express";
import cors from "cors";
import logger from "morgan";
import path from "path";
const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.get("/hello", (_, response) => {
  response.send({
    message: "Hello from server",
  });
});

app.get("/api/data", (_, response) => {
  response.send({
    data: [
      {
        id: 1,
        name: "Deepanshu",
        dept: "Software",
      },
      {
        id: 2,
        name: "Deep2",
        dept: "Automation",
      },
    ],
  });
});
app.use(express.static(path.join(__dirname, "/../client-app/build")));
app.get("*", function (_, res) {
  res.sendFile(path.join(__dirname, "/../client-app/build", "index.html"));
});

app.listen(5000, () => {
  console.log("Server listening at port 5000");
});
