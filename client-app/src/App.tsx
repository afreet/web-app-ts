import React, { useState, useEffect } from "react";
import "./App.css";
import ChildComponent from "./components/ChildComponent";
import Data from "./models/data";

const App = () => {
  const [state, setState] = useState({
    data: [] as Data[],
  });
  const getData = async () => {
    const response = await fetch("/api/data");
    const data = await response.json();
    setState(data);
  };
  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      {state && state.data.length > 0 ? (
        state.data.map((value) => (
          <ChildComponent key={value.id} value={value} />
        ))
      ) : (
        <div>No data...</div>
      )}
    </>
  );
};

export default App;
