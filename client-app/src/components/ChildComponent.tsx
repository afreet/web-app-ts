import Data from "src/models/data";
import React from "react";

export interface ChildComponentProps {
  value: Data;
}

const ChildComponent: React.FunctionComponent<ChildComponentProps> = ({
  value,
}) => {
  return (
    <div>
      <h6>{value.name}</h6>
      <p>{value.dept}</p>
    </div>
  );
};

export default ChildComponent;
